package com.smilne;

public class Calculator
{
	private int value = 0;

	public int getValue ()
	{
		return value;
	}

	public void add ()
	{
		add(1);
	}

	public void add ( int value )
	{
		this.value += value;
	}

	public void subtract ()
	{
		subtract(1);
	}

	public void subtract ( int value )
	{
		this.value -= value;
	}
}
