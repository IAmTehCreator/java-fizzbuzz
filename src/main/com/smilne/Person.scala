package com.smilne

class Person(
	val forename: String,
	val surname: String
) {

	def name() =
		s"$forename $surname"
}
