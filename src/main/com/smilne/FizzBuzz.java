package com.smilne;

import java.util.ArrayList;

public final class FizzBuzz
{
	private static final String APP_TITLE = "FizzBuzz";

	private static final ITransformer FIZZ = (ITransformer) new MultipleTransformer(3, "Fizz");
	private static final ITransformer BUZZ = (ITransformer) new MultipleTransformer(5, "Buzz");
	private static final ITransformer JAZZ = (ITransformer) new PrimeTransformer("Jazz");

	/**
	 * Application entry point.
	 * @param args The arguments provied by the calling application
	 */
	public static void main(String[] args)
	{
		System.out.println(APP_TITLE + "\n");

		for(int i = 0; i <= 100; i++)
		{
			System.out.println( fizzbuzz(i) );
		}
	}

	/**
	 * Takes a number and returns "Fizz" if the number is a multiple of 3
	 * @param value Pick a number, any number
	 * @return If the provided value was a multiple of three then "Fizz" will be
	 *         returned, otherwise the value will be returned as a string
	 */
	public static String fizz(int value)
	{
		return transform(value, FIZZ);
	}

	/**
	 * Takes a number and returns "Buzz" if the number is a multiple of 5
	 * @param value Pick a number, any number
	 * @return If the provided value was a multiple of five then "Buzz" will be
	 *         returned, otherwise the value will be returned as a string
	 */
	public static String buzz(int value)
	{
		return transform(value, BUZZ);
	}

	/**
	 * Takes a number and returns "Jazz" if the number is a prime number.
	 * @param value Pick a number, any number
	 * @return If the provided value is a prime then "Jazz" will be returned,
	 *         otherwise the value will be returned as a string
	 */
	public static String jazz(int value)
	{
		return transform(value, JAZZ);
	}

	/**
	 * Takes a number and returns "Fizz" if the number is a multiple of 3, "buzz"
	 * if the number is a multiple of 5 or "FizzBuzz" if it is a multiple of both.
	 * @param value Pick a number, any number
	 * @return The string "Fizz", "Buzz" or "FizzBuzz" if the value is a multiple
	 *         of 3, 5 or both. If the value is not a multiple of either then the
	 *         stringified number is returned
	 */
	public static String fizzbuzz(int value)
	{
		ArrayList<ITransformer> transformers = new ArrayList<ITransformer>();
		transformers.add(FIZZ);
		transformers.add(BUZZ);

		return transform(value, transformers);
	}

	/**
	 * Takes a number and returns "Fizz" if the number is a multiple of 3, "buzz"
	 * if the number is a multiple of 5 or "FizzBuzz" if it is a multiple of both.
	 * Also appends or returns "Jazz" if the number is a prime number.
	 * @param value Pick a number, any number
	 * @return The number transformed into a string
	 */
	public static String fizzbuzzjazz(int value)
	{
		ArrayList<ITransformer> transformers = new ArrayList<ITransformer>();
		transformers.add(FIZZ);
		transformers.add(BUZZ);
		transformers.add(JAZZ);

		return transform(value, transformers);
	}

	/**
	 * Uses the given transformer to conditionally transform the specified number.
	 * @param value The number to be transformed
	 * @param transformer The transformer to use to transform the number
	 * @return The value returned from the transformer
	 */
	private static String transform(int value, ITransformer transformer)
	{
		ArrayList<ITransformer> transformers = new ArrayList<ITransformer>();
		transformers.add(transformer);

		return transform(value, transformers);
	}

	/**
	 * Takes a number and passes it through the list of transformers provided. The
	 * concatenated output of the transformers is returned.
	 * @param value The number to be transformed
	 * @param transformers A collection of transformers to pass the value through
	 * @return If none of the transformers changed the number then the string value
	 *         of the number will be returned. If one or more transformers changed
	 *         the number then the outputs of the transformers are concatenated
	 *         and returned.
	 */
	private static String transform(int value, ArrayList<ITransformer> transformers)
	{
		String valueStr = String.valueOf(value);
		ArrayList<String> messages = new ArrayList<String>();

		if( value == 0 )
		{
			return valueStr;
		}

		for(ITransformer transformer : transformers)
		{
			String result = transformer.transform(value);
			if( !result.equals(valueStr) )
			{
				messages.add(result);
			}
		}

		return messages.isEmpty() ? valueStr : String.join("", messages);
	}

	/**
	 * An interface for classes that can be used to transform an integer number
	 * into a string.
	 */
	private interface ITransformer
	{
		String transform(int value);
	}

	/**
	 * Transformer class which transforms numbers conditionally into a message if
	 * they are a multiple of the target number.
	 */
	private final static class MultipleTransformer implements ITransformer
	{
		private int target;
		private String message;

		/**
		 * Constructor which sets the target number to test for multiples and the
		 * message to be returned when a value is a multiple.
		 * @param target The number to test given values against being a multiple
		 * @param message The message to return when a value is a multiple of the target
		 */
		public MultipleTransformer(int target, String message)
		{
			this.target = target;
			this.message = message;
		}

		/**
		 * Takes a number and checks if it is a multiple of the target number, if
		 * it is then the configured message is returned otherwise the number is
		 * cast to a string and returned.
		 * @param value The number to test against the target
		 * @return The message if the provided value is a multiple of the target
		 *         otherwise the value is stringified and returned
		 */
		public String transform(int value)
		{
			return isMultiple(value) ? message : String.valueOf(value);
		}

		/**
		 * Checks if the given value is a multiple of the target number.
		 * @param value The number to check against the target
		 * @return True if the number is a multiple of the target, false otherwise
		 */
		private Boolean isMultiple(int value)
		{
			return value % target == 0;
		}
	}

	/**
	 * Transformer class which transforms numbers conditionally into a message if
	 * they are a prime number.
	 */
	private final static class PrimeTransformer implements ITransformer
	{
		private String message;

		/**
		 * Constructor which sets the message to return if the value is prime
		 * @param message The message to return when a value is prime
		 */
		public PrimeTransformer(String message)
		{
			this.message = message;
		}

		/**
		 * Takes a number and checks if it is a prime number, if it is then the
		 * message is returned otherwise the number is returned.
		 * @param value The number to test against the target
		 * @return The message if the provided value is prime otherwise the
		 *         value is stringified and returned
		 */
		public String transform(int value)
		{
			return isPrime(value) ? message : String.valueOf(value);
		}

		/**
		 * Checks if the given value is a prime number.
		 * @param value The number to check for prime
		 * @return True if the number is a prime number, false otherwise
		 */
		private Boolean isPrime(int value)
		{
			for( int divisor = 2; divisor <= value / 2; divisor++)
			{
				if ( value % divisor == 0 )
				{
					return false;
				}
			}

			return true;
		}
	}
}
