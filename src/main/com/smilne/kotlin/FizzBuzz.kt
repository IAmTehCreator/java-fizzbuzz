package com.smilne.kotlin

class FizzBuzz {

	interface ITransformer {
		fun transform(value: Int): String?
	}

	private val FIZZ: ITransformer = MultipleTransformer(3, "Fizz")
	private val BUZZ: ITransformer = MultipleTransformer(5, "Buzz")
	private val JAZZ: ITransformer = PrimeTransformer("Jazz")

	fun fizz(value: Int): String = transform(value, FIZZ)
	fun buzz(value: Int): String = transform(value, BUZZ)
	fun jazz(value: Int): String = transform(value, JAZZ)

	fun fizzbuzz(value: Int): String = transform(value, arrayOf(FIZZ, BUZZ))
	fun fizzbuzzjazz(value: Int): String = transform(value, arrayOf(FIZZ, BUZZ, JAZZ))

	private fun transform(value: Int, transformer: ITransformer): String = transform(value, arrayOf(transformer))
	private fun transform(value: Int, transformers: Array<ITransformer>): String =
		if(value == 0) value.toString()
		else transform(value, transformers.mapNotNull({ item -> item.transform(value) }))

	private fun transform(value: Int, results: List<String>): String =
		if(results.size == 0) value.toString()
		else results.joinToString()

	class MultipleTransformer(val target: Int, val message: String): ITransformer {

		override fun transform( value: Int): String? = if ( isMultiple(value) ) message else null

		private fun isMultiple( value: Int ): Boolean = value % target == 0
	}

	class PrimeTransformer(val message: String): ITransformer {

		override fun transform(value: Int): String? = if ( isPrime(value) ) message else null

		private fun isPrime(value: Int): Boolean = isPrime(value, 2)
		private fun isPrime(value: Int, divisor: Int): Boolean =
			if(isMultiple(value, divisor)) false
			else if(isHalf(value, divisor)) true
			else isPrime(value, divisor + 1)

		private fun isMultiple(value: Int, target: Int): Boolean = value % target == 0
		private fun isHalf(value: Int, target: Int): Boolean = target <= value / 2
	}
}
