package com.smilne

open class Vehicle(
	val make: String,
	val model: String,
	val year: Int
) {

	/**
	 * Serialises this vehicle to a string
	 * @return The vehicle make and model
	 */
	override fun toString(): String = "${year.toString()} $make $model"
}
