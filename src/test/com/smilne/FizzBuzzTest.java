package com.smilne;

import org.junit.*;

import com.smilne.FizzBuzz;

public class FizzBuzzTest
{
	@Test
	public void fizz_givenNotMultipleOfThree_returnsValue()
	{
		Assert.assertEquals("1", FizzBuzz.fizz(1));
		Assert.assertEquals("-2", FizzBuzz.fizz(-2));
		Assert.assertEquals("482", FizzBuzz.fizz(482));
	}

	@Test
	public void fizz_givenMultipleOfThree_returnsFizz()
	{
		String fizz = "Fizz";

		Assert.assertEquals(fizz, FizzBuzz.fizz(3));
		Assert.assertEquals(fizz, FizzBuzz.fizz(-12));
		Assert.assertEquals(fizz, FizzBuzz.fizz(753));
	}

	@Test
	public void buzz_givenNotMultipleOfFive_returnsValue()
	{
		Assert.assertEquals("4", FizzBuzz.buzz(4));
		Assert.assertEquals("-64", FizzBuzz.buzz(-64));
		Assert.assertEquals("2169", FizzBuzz.buzz(2169));
	}

	@Test
	public void buzz_givenMultipleOfFive_returnsBuzz()
	{
		String buzz = "Buzz";

		Assert.assertEquals(buzz, FizzBuzz.buzz(5));
		Assert.assertEquals(buzz, FizzBuzz.buzz(-60));
		Assert.assertEquals(buzz, FizzBuzz.buzz(2615));
	}

	@Test
	public void fizzbuzz_notMultipleOfThree_notMultipleOfFive_returnsValue()
	{
		Assert.assertEquals("Should return value when it is not a multiple of 3 or 5", "4", FizzBuzz.fizzbuzz(4));
		Assert.assertEquals("Should return value when it is zero", "0", FizzBuzz.fizzbuzz(0));
		Assert.assertEquals("Should return value when it is not a multiple of 3 or 5 and is large", "2461", FizzBuzz.fizzbuzz(2461));
	}

	@Test
	public void fizzbuzz_multipleOfThree_notMultipleOfFive_returnsFizz()
	{
		String fizz = "Fizz";

		Assert.assertEquals("Should return 'Fizz' when value is multiple itself", fizz, FizzBuzz.fizzbuzz(3));
		Assert.assertEquals("Should return 'Fizz' when value is multiple and negative", fizz, FizzBuzz.fizzbuzz(-12));
		Assert.assertEquals("Should return 'Fizz' when value is multiple and large", fizz, FizzBuzz.fizzbuzz(753));
	}

	@Test
	public void fizzbuzz_notMultipleOfThree_multipleOfFive_returnsBuzz()
	{
		String buzz = "Buzz";

		Assert.assertEquals("Should return 'Buzz' when value is multiple itself", buzz, FizzBuzz.fizzbuzz(5));
		Assert.assertEquals("Should return 'Buzz' when value is multiple and negative", buzz, FizzBuzz.fizzbuzz(-65));
		Assert.assertEquals("Should return 'Buzz' when value is multiple and large", buzz, FizzBuzz.fizzbuzz(2615));
	}

	@Test
	public void fizzbuzz_multipleOfThree_multipleOfFive_returnsFizzBuzz()
	{
		String fizzbuzz = "FizzBuzz";

		Assert.assertEquals("Should return 'FizzBuzz' when value is multiple of 3 and 5", fizzbuzz, FizzBuzz.fizzbuzz(15));
		Assert.assertEquals("Should return 'FizzBuzz' when value is multiple of 3 and 5 and negative", fizzbuzz, FizzBuzz.fizzbuzz(-60));
		Assert.assertEquals("Should return 'FizzBuzz' when value is multiple of 3 and 5 and large", fizzbuzz, FizzBuzz.fizzbuzz(1350));
	}

	@Test
	public void jazz_notPrime_returnsValue()
	{
		Assert.assertEquals("4", FizzBuzz.jazz(4));
		Assert.assertEquals("62", FizzBuzz.jazz(62));
		Assert.assertEquals("1862", FizzBuzz.jazz(1862));
	}

	@Test
	public void jazz_prime_returnsJazz()
	{
		String jazz = "Jazz";

		Assert.assertEquals(jazz, FizzBuzz.jazz(2));
		Assert.assertEquals(jazz, FizzBuzz.jazz(-62));
		Assert.assertEquals(jazz, FizzBuzz.jazz(271));
	}

	@Test
	public void fizzbuzzjazz_not3_not5_notPrime_returnsValue()
	{
		Assert.assertEquals("4", FizzBuzz.fizzbuzzjazz(4));
	}

	@Test
	public void fizzbuzzjazz_3_not5_notPrime_returnsFizz()
	{
		Assert.assertEquals("Fizz", FizzBuzz.fizzbuzzjazz(9));
	}

	@Test
	public void fizzbuzzjazz_not3_5_notPrime_returnsBuzz()
	{
		Assert.assertEquals("Buzz", FizzBuzz.fizzbuzzjazz(20));
	}

	@Test
	public void fizzbuzzjazz_not3_not5_prime_returnsJazz()
	{
		Assert.assertEquals("Jazz", FizzBuzz.fizzbuzzjazz(37));
	}

	@Test
	public void fizzbuzzjazz_3_5_notPrime_returnsFizzBuzz()
	{
		Assert.assertEquals("FizzBuzz", FizzBuzz.fizzbuzzjazz(15));
	}

	@Test
	public void fizzbuzzjazz_3_not5_prime_returnsFizzJazz()
	{
		Assert.assertEquals("FizzJazz", FizzBuzz.fizzbuzzjazz(3));
	}

	@Test
	public void fizzbuzzjazz_not3_5_prime_returnsBuzzJazz()
	{
		Assert.assertEquals("BuzzJazz", FizzBuzz.fizzbuzzjazz(5));
	}
}
