package com.smilne;

import org.junit.*;
import com.smilne.Calculator;

public class CalculatorTest
{
	private Calculator calc;

	@Before
	public void beforeEach() throws Exception
	{
		calc = new Calculator();
	}

	@Test
	public void constructor_shouldInitialiseValeToZero()
	{
		Assert.assertEquals("Should initialise value to zero", 0, calc.getValue());
	}

	@Test
	public void add_withoutArguments_shouldAdd1ToValue()
	{
		calc.add();
		Assert.assertEquals("Should add 1 to value by default", 1, calc.getValue());

		calc.add();
		Assert.assertEquals("Should add 1 to the previous value", 2, calc.getValue());

		calc.add();
		Assert.assertEquals("Should add 1 to the previous value", 3, calc.getValue());
	}

	@Test
	public void add_withArgument_shouldAddArgumentToValue()
	{
		calc.add(3);
		Assert.assertEquals("Should add 3 to the initial value", 3, calc.getValue());

		calc.add(2);
		Assert.assertEquals("Should add 2 to the previous value", 5, calc.getValue());

		calc.add(1);
		Assert.assertEquals("Should add 1 to the previous value", 6, calc.getValue());
	}

	@Test
	public void subtract_withoutArguments_shouldSubtract1FromValue()
	{
		calc.subtract();
		Assert.assertEquals("Should subtract 1 from value by default", -1, calc.getValue());

		calc.subtract();
		Assert.assertEquals("Should subtract 1 from the previous value", -2, calc.getValue());

		calc.subtract();
		Assert.assertEquals("Should subtract 1 from the previous value", -3, calc.getValue());
	}

	@Test
	public void subtract_withArgument_shouldSubtractArgumentFromValue()
	{
		calc.subtract(3);
		Assert.assertEquals("Should subtract 3 from value by default", -3, calc.getValue());

		calc.subtract(2);
		Assert.assertEquals("Should subtract 2 from the previous value", -5, calc.getValue());

		calc.subtract(1);
		Assert.assertEquals("Should subtract 1 from the previous value", -6, calc.getValue());
	}
}
