package com.smilne.kotlin

import org.junit.Assert
import org.junit.Test
import com.smilne.kotlin.FizzBuzz

class FizzBuzzTest {

    private val FIZZ = "Fizz"
    private val BUZZ = "Buzz"
    private val JAZZ = "Jazz"

    private fun fizz(expected: String, input: Int) = Assert.assertEquals(expected, FizzBuzz().fizz(input))
    private fun buzz(expected: String, input: Int) = Assert.assertEquals(expected, FizzBuzz().buzz(input))
    private fun jazz(expected: String, input: Int) = Assert.assertEquals(expected, FizzBuzz().jazz(input))

    @Test fun fizz_givenZero_returnsZero() = fizz("0", 0)
    @Test fun fizz_givenThree_returnsFizz() = fizz(FIZZ, 3)
    @Test fun fizz_givenNonMultipleOfThree_returnsNumber() = fizz("77", 77)
    @Test fun fizz_givenMultipleOfThree_returnsFizz() = fizz(FIZZ, 66)

    @Test fun buzz_givenZero_returnsZero() = buzz("0", 0)
    @Test fun buzz_givenFive_returnsBuzz() = buzz(BUZZ, 5)
    @Test fun buzz_givenNonMultipleOfFive_returnsNumber() = buzz("77", 77)
    @Test fun buzz_givenMultipleOfFive_returnsBuzz() = buzz(BUZZ, 145)

    @Test fun jazz_givenZero_returnsZero() = jazz("0", 0)
    @Test fun jazz_givenPrime_returnsJazz() = jazz(JAZZ, 23879)
    @Test fun jazz_givenNonPrime_returnsNumber() = jazz("2222", 2222)

}
