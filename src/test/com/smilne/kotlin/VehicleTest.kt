package com.smilne.kotlin

import org.junit.*
import com.smilne.Vehicle

class VehicleTest {

	@Test fun toString_shouldReturnYearMakeAndModel() {
		val vehicle = Vehicle("Vauxhall", "Insignia", 2016)

		Assert.assertEquals("2016 Vauxhall Insignia", vehicle.toString())
	}
}
