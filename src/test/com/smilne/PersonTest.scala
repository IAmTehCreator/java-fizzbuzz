package com.smilne;

import org.junit._;

class PersonTest {

	@Test def greet_returnsForenameAndSurname() {
		val person = new Person("John", "Smith");

		Assert.assertEquals("John Smith", person.name);
	}

}
