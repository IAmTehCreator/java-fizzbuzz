# Java FizzBuzz Example
This repository contains a few example programs written with a TDD approach within Java using JUnit. The following example implementations are included;

* Simple calculator for addition & subtraction
* FizzBuzz implementation, supporting different types of conditions

## Building
To build this repository you will need [Apache Ant](https://ant.apache.org/). The application can be compiled and the tests run against it using the `ant test` command in the terminal. The following build targets are supported;

* `ant clean` - Removes temporary build files
* `ant compile` - Compiles the application source code into the `bin/` directory
* `ant package` - Packages the `bin/` directory into a `.jar` file
* `ant test` - Runs the unit tests against the packaged `.jar` file and opens a report with the results

## License
The license for this source code is contained in the `LICENSE` file. TL;DR: It's an MIT license, do whatever you want with it.
